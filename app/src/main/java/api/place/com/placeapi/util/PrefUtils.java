package api.place.com.placeapi.util;

import android.content.SharedPreferences.Editor;

public class PrefUtils {
    public static int getInt(String key, int defValue) {
        return Constant.preferences.getInt(key, defValue);
    }

    public static String getString(String key, String defValue) {
        return Constant.preferences.getString(key, defValue);
    }

    public static void putInt(String key, int defValue) {
        Editor editor = Constant.preferences.edit();
        editor.putInt(key, defValue);
        editor.commit();
    }

    public static void putString(String key, String defValue) {
        Editor editor = Constant.preferences.edit();
        editor.putString(key, defValue);
        editor.commit();
    }

    public static void putBoolean(String key, boolean defValue) {
        Editor editor = Constant.preferences.edit();
        editor.putBoolean(key, defValue);
        editor.commit();
    }

    public static boolean getBoolean(String key, boolean defValue) {
        return Constant.preferences.getBoolean(key, defValue);
    }

    public static void clearData() {
        Editor editor = Constant.preferences.edit();
        editor.clear();
        editor.commit();
    }
}
