package api.place.com.placeapi.model;

import android.graphics.Bitmap;

import com.google.api.client.util.Key;

import java.io.Serializable;

/**
 * Created by VuVan on 17/02/2017.
 */

public class Photo implements Serializable{
    @Key
    public int height;

    @Key
    public String photo_reference;

    @Key
    public int width;

    @Key
    public Bitmap bitmapPhoto;

}
