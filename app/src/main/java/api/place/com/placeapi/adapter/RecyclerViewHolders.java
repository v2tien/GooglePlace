package api.place.com.placeapi.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import api.place.com.placeapi.R;


/**
 * Created by VuVan on 20/08/2016.
 */
public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imvPhoto;
    public TextView tvName, tvAddress;
    public ViewHolderClicks holderClicks;

    public RecyclerViewHolders(View convertView, ViewHolderClicks holderClicks) {
        super(convertView);
        this.holderClicks = holderClicks;
        itemView.setOnClickListener(this);

        imvPhoto = (ImageView) convertView.findViewById(R.id.imv_photo);
        tvName = (TextView)convertView.findViewById(R.id.tv_name);
        tvAddress = (TextView)convertView.findViewById(R.id.tv_address);
    }

    @Override
    public void onClick(View v) {
        if(holderClicks != null){
            holderClicks.clickFavorite(getPosition());
        }
    }

    public interface ViewHolderClicks {
        public void clickFavorite(int position);
    }
}
