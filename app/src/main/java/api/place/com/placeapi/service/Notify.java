package api.place.com.placeapi.service;

/**
 * Created by VuVan on 15/07/2016.
 */
public enum Notify {

    FAVORITE_PLACE("api.place.com.placeapi.service.notify.favorite"),
    UNFAVORITE_PLACE("api.place.com.placeapi.service.notify.unfavorite");

    private String value;

    private Notify(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
