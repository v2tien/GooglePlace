package api.place.com.placeapi.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import api.place.com.placeapi.R;
import api.place.com.placeapi.adapter.ViewPagerAdapter;
import api.place.com.placeapi.fragment.FavoriteFragment;
import api.place.com.placeapi.fragment.MapPlaceFragment;
import api.place.com.placeapi.util.PrefUtils;

public class MainActivity extends AppCompatActivity {

    private int TAB_HOME = 0;
    private int TAB_CURRENT = 1;

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponent();
        initViewPager();
    }

    private void initComponent(){
        viewPager = (ViewPager)findViewById(R.id.view_pager);
        tabLayout = (TabLayout)findViewById(R.id.tabs);
    }

    // Create View Pager
    private void initViewPager() {
        viewPager.setOffscreenPageLimit(2);
        viewPager.setEnabled(true);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new MapPlaceFragment(), getString(R.string.tab_1));
        viewPagerAdapter.addFragment(new FavoriteFragment(), getString(R.string.tab_2));

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        View tabHome = LayoutInflater.from(this).inflate(R.layout.tab_view_pager, null);
        TextView tvTitle = (TextView) tabHome.findViewById(R.id.tv_title);
        tvTitle.setText(getString(R.string.tab_1));
        tvTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        tabLayout.getTabAt(TAB_HOME).setCustomView(tabHome);

        View tabCurrent = LayoutInflater.from(this).inflate(R.layout.tab_view_pager, null);
        TextView tvTitle1 = (TextView) tabCurrent.findViewById(R.id.tv_title);
        tvTitle1.setText(getString(R.string.tab_2));
        tabLayout.getTabAt(TAB_CURRENT).setCustomView(tabCurrent);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                TextView tvTitle = (TextView) tab.getCustomView().findViewById(R.id.tv_title);
                // Text color when selected
                tvTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
                if (tab.getPosition() == TAB_HOME) {

                } else if (tab.getPosition() == TAB_CURRENT) {

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // Text color when unselected
                TextView tvTitle = (TextView) tab.getCustomView().findViewById(R.id.tv_title);
                tvTitle.setTextColor(getResources().getColor(R.color.dark));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PrefUtils.clearData();
        Log.e("onDestroy", "onDestroy");
    }
}
