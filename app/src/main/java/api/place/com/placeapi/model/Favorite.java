package api.place.com.placeapi.model;

import android.graphics.Bitmap;

/**
 * Created by VuVan on 17/02/2017.
 */

public class Favorite {

    private String name;
    private String address;
    private String reference;
    private Bitmap bitmap;
    private String referencePhoto;
    private int widthPhoto;
    private int heightPhoto;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getReferencePhoto() {
        return referencePhoto;
    }

    public void setReferencePhoto(String referencePhoto) {
        this.referencePhoto = referencePhoto;
    }

    public int getWidthPhoto() {
        return widthPhoto;
    }

    public void setWidthPhoto(int widthPhoto) {
        this.widthPhoto = widthPhoto;
    }

    public int getHeightPhoto() {
        return heightPhoto;
    }

    public void setHeightPhoto(int heightPhoto) {
        this.heightPhoto = heightPhoto;
    }
}
