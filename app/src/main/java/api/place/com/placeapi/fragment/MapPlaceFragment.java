package api.place.com.placeapi.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import api.place.com.placeapi.R;
import api.place.com.placeapi.activity.PlaceDetailActivity;
import api.place.com.placeapi.data.GooglePlaces;
import api.place.com.placeapi.model.Photo;
import api.place.com.placeapi.model.Place;
import api.place.com.placeapi.model.PlacesList;
import api.place.com.placeapi.util.Common;
import api.place.com.placeapi.util.Constant;
import api.place.com.placeapi.util.GPSTracker;

import static com.google.android.gms.internal.zzir.runOnUiThread;

/**
 * Created by VuVan on 16/02/2017.
 */

public class MapPlaceFragment extends Fragment {

    private MapView mMapView;
    private GoogleMap googleMap;
    private double longitude;
    private double latitude;
    private GPSTracker gps;
    private Button btnShowPlace;

    private ProgressDialog pDialog;

    private GooglePlaces googlePlaces;
    // Places List
    private PlacesList nearPlaces;

    private ArrayList<HashMap<String, String>> placesListItems = new ArrayList<>();

    // Map overlay items
    private List<Overlay> mapOverlays;

    private GeoPoint geoPoint;
    // Map controllers
    private MapController mc;
    private OverlayItem overlayitem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) layout.findViewById(R.id.map);
        btnShowPlace = (Button) layout.findViewById(R.id.btn_show_place);

        gps = new GPSTracker(getActivity());
        mMapView.onCreate(savedInstanceState);

        viewMap();
        initControl();
        return layout;
    }

    private void viewMap() {
        mMapView.onResume();
        // show map
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                googleMap.setMyLocationEnabled(true);
                if (gps.canGetLocation()) {
                    longitude = gps.getLongitude();
                    latitude = gps.getLatitude();

                    // For dropping a marker at a point on the Map
                    LatLng sydney = new LatLng(latitude, longitude);
//                googleMap.setOnMyLocationChangeListener(myLocationChangeListener);
                    googleMap.addMarker(new MarkerOptions().position(sydney).title("My location")).showInfoWindow();

                    // For zooming automatically to the location of the marker
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(13).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    // Event click item place on map
                    googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            // TODO: 16/02/2017  click detail place
                            String reference = null;
                            String name = "";
                            String address = "";
                            String referencePhoto = "";
                            String height = "0";
                            String width = "0";
                            // Check name of place selected and name of place in list all place
                            for (HashMap<String, String> hashMap : placesListItems) {
                                if (hashMap.get(Constant.KEY_NAME).equalsIgnoreCase(marker.getTitle())) {
                                    reference = hashMap.get(Constant.KEY_REFERENCE);
                                    name = hashMap.get(Constant.KEY_NAME);
                                    address = hashMap.get(Constant.KEY_VICINITY);
                                    // Check data
                                    if (referencePhoto != null) {
                                        referencePhoto = hashMap.get(Constant.KEY_REFERENCE_PHOTO);
                                    }
                                    if (width != null) {
                                        width = hashMap.get(Constant.KEY_WIDTH);
                                    }
                                    if (height != null) {
                                        height = hashMap.get(Constant.KEY_HEIGHT);
                                    }
                                }
                            }
                            // Intent data to class detail
                            if (reference != null) {
                                Intent intent = new Intent(getActivity(), PlaceDetailActivity.class);
                                intent.putExtra(Constant.KEY_REFERENCE, reference);
                                intent.putExtra(Constant.KEY_NAME, name);
                                intent.putExtra(Constant.KEY_REFERENCE_PHOTO, referencePhoto);
                                intent.putExtra(Constant.KEY_WIDTH, width);
                                intent.putExtra(Constant.KEY_HEIGHT, height);
                                intent.putExtra(Constant.KEY_ADDRESS, address);
                                startActivity(intent);
                            }
                        }
                    });
                }
            }
        });
    }

    private void initControl() {
        // button click show all place
        btnShowPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Clear all place on map
                googleMap.clear();
                // Check network
                if (Common.checkNetwork(getActivity())) {
                    new LoadPlaces().execute();
                } else {
                    Toast.makeText(getActivity(), R.string.no_connection,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    class LoadPlaces extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage(getString(R.string.loading));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            // creating Places class object
            googlePlaces = new GooglePlaces();
            try {
//                Check list of supported types of places. https://developers.google.com/places/supported_types
                String types = "cafe|restaurants";
                // Radius in meters - increase this value if you don't find any places
                double radius = 3000; // 3000 meters
                // get nearest places
                nearPlaces = googlePlaces.search(gps.getLatitude(),
                        gps.getLongitude(), radius, types);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    String status = nearPlaces.status;
                    // Check for all possible status
                    if (status.equals("OK")) {
                        // Successfully got places details
                        if (nearPlaces.results != null) {
                            // loop through each place
                            for (Place p : nearPlaces.results) {
                                MarkerOptions markerOptions = new MarkerOptions();

                                HashMap<String, String> map = new HashMap<>();
                                map.put(Constant.KEY_REFERENCE, p.reference);
                                // Place name
                                map.put(Constant.KEY_NAME, p.name);
                                // Place id
                                map.put(Constant.PLACE_ID, p.id);
                                // Vincinity
                                map.put(Constant.KEY_VICINITY, p.vicinity);
                                // List photos
                                List<Photo> photoList = p.photos;

                                //Check list photo null or empty
                                if (photoList == null || photoList.isEmpty()) {
                                } else {
                                    map.put(Constant.KEY_HEIGHT, String.valueOf(p.photos.get(0).height));
                                    map.put(Constant.KEY_REFERENCE_PHOTO, p.photos.get(0).photo_reference);
                                    map.put(Constant.KEY_WIDTH, String.valueOf(p.photos.get(0).width));
                                }
                                // get location of place
                                double latitude = p.geometry.location.lat;
                                double longitude = p.geometry.location.lng;
                                geoPoint = new GeoPoint((int) (latitude * 1E6),
                                        (int) (longitude * 1E6));

                                // adding HashMap to ArrayList
                                placesListItems.add(map);

                                // Show info place when click place
                                LatLng latLng = new LatLng(latitude, longitude);
                                markerOptions.position(latLng);
                                markerOptions.title(p.name).snippet(p.vicinity);
                                googleMap.addMarker(markerOptions);
                            }
                        }
                    } else if (status.equals("ZERO_RESULTS")) {
                        // Zero results found
                        Toast.makeText(getActivity(),
                                "Sorry no places found. Try to change the types of places", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("UNKNOWN_ERROR")) {
                        Toast.makeText(getActivity(),
                                "Sorry unknown error occured.", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("OVER_QUERY_LIMIT")) {
                        Toast.makeText(getActivity(),
                                "Sorry query limit to google places is reached", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("REQUEST_DENIED")) {
                        Toast.makeText(getActivity(),
                                "Sorry error occured. Request is denied", Toast.LENGTH_SHORT).show();
                    } else if (status.equals("INVALID_REQUEST")) {
                        Toast.makeText(getActivity(),
                                "Sorry error occured. Invalid Request", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(),
                                "Sorry error occured.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
