package api.place.com.placeapi.service;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import api.place.com.placeapi.util.Constant;

/**
 * Created by VuVan on 17/02/2017.
 */

public class NotifyService {

    public static void notifyFavorite(String name, String address, String reference, String refPhoto, int width, int height) {
        Intent intent = new Intent(Notify.FAVORITE_PLACE.getValue());
        intent.putExtra(Constant.KEY_NAME, name);
        intent.putExtra(Constant.KEY_ADDRESS, address);
        intent.putExtra(Constant.KEY_REFERENCE, reference);
//        intent.putExtra(Constant.KEY_BITMAP, bitmap);
        intent.putExtra(Constant.KEY_REFERENCE_PHOTO, refPhoto);
        intent.putExtra(Constant.KEY_WIDTH, width);
        intent.putExtra(Constant.KEY_HEIGHT, height);
        LocalBroadcastManager.getInstance(Constant.context).sendBroadcast(intent);
    }

    public static void notifyUnFavorite(String name, String address) {
        Intent intent = new Intent(Notify.UNFAVORITE_PLACE.getValue());
        intent.putExtra(Constant.KEY_NAME, name);
        intent.putExtra(Constant.KEY_ADDRESS, address);
        LocalBroadcastManager.getInstance(Constant.context).sendBroadcast(intent);
    }
}
