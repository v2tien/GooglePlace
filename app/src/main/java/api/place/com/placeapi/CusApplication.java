package api.place.com.placeapi;

import android.app.Application;
import android.content.Context;

import api.place.com.placeapi.util.Constant;

/**
 * Created by VuVan on 17/02/2017.
 */

public class CusApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Constant.context = getApplicationContext();
        Constant.preferences = getSharedPreferences(Constant.PREF_APP, Context.MODE_PRIVATE);
    }
}
