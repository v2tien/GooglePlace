package api.place.com.placeapi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import api.place.com.placeapi.R;
import api.place.com.placeapi.data.GooglePlaces;
import api.place.com.placeapi.model.Photo;
import api.place.com.placeapi.model.PlaceDetails;
import api.place.com.placeapi.service.NotifyService;
import api.place.com.placeapi.util.Common;
import api.place.com.placeapi.util.Constant;
import api.place.com.placeapi.util.PrefUtils;

/**
 * Created by VuVan on 16/02/2017.
 */

public class PlaceDetailActivity extends AppCompatActivity {

    private TextView tvName, tvAddress;
    private ActionBar actionBar;
    private ImageView imgView;

    // Google Places
    private GooglePlaces googlePlaces;

    // Place Details
    private PlaceDetails placeDetails;
    private List<Photo> photos = new ArrayList<>();
    private List<Bitmap> lstBitmap = new ArrayList<>();

    // Progress dialog
    private ProgressDialog pDialog;
    private Bitmap bitmapPhoto = null;

    private String reference = "";
    private String name = "";
    private String address = "";
    private int typeIntent;
    private String referencePhoto = "";
    private String with, height;

    private MenuItem btnFavorite;
    private boolean isFavorite = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);

        // Get data intent
        Intent intent = getIntent();
        reference = intent.getStringExtra(Constant.KEY_REFERENCE);
        name = intent.getStringExtra(Constant.KEY_NAME);
        address = intent.getStringExtra(Constant.KEY_ADDRESS);
        referencePhoto = intent.getStringExtra(Constant.KEY_REFERENCE_PHOTO);
        with = intent.getStringExtra(Constant.KEY_WIDTH);
        height = intent.getStringExtra(Constant.KEY_HEIGHT);
        // Check size width, height of image place
        if (with == null) {
            with = "0";
        }
        if (height == null) {
            height = "0";
        }
        // Get data from cache for check favorite
        isFavorite = PrefUtils.getBoolean(name, false);

        initActionBar();
        initComponent();
    }

    // Set info for actionbar
    private void initActionBar() {
        actionBar = getSupportActionBar();
        actionBar.setElevation(0);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(R.string.place_detail);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favorite, menu);
        btnFavorite = menu.findItem(R.id.action_favorite);
        //Show icon when favorite or unfavorite
        if (isFavorite == true) {
            btnFavorite.setIcon(R.drawable.ic_star_yellow);
        } else {
            btnFavorite.setIcon(R.drawable.ic_star_white);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Click back
                onBackPressed();
                break;
            case R.id.action_favorite:
                if (isFavorite == false) {
                    // Click favorite place
                    btnFavorite.setIcon(R.drawable.ic_star_yellow);
                    PrefUtils.putBoolean(name, true);
                    NotifyService.notifyFavorite(name, address, reference, referencePhoto,
                            Integer.parseInt(with), Integer.parseInt(height));
                } else {
                    // Click unfavorite place
                    btnFavorite.setIcon(R.drawable.ic_star_white);
                    PrefUtils.putBoolean(name, false);
                    NotifyService.notifyUnFavorite(name, address);
                }
                break;
        }
        return true;
    }

    private void initComponent() {
        tvName = (TextView) findViewById(R.id.tv_name);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        imgView = (ImageView) findViewById(R.id.imv);
        // Get link image of place
        String Url = GooglePlaces.getLinkPhoto(referencePhoto, Integer.parseInt(with), Integer.parseInt(height));
        if (Integer.parseInt(with) != 0 && Integer.parseInt(height) != 0) {
            // Check network
            if (Common.checkNetwork(PlaceDetailActivity.this)) {
                new ImageDownloadTask().execute(Url);
            } else {
                Toast.makeText(PlaceDetailActivity.this, R.string.no_connection,
                        Toast.LENGTH_SHORT).show();
            }
        }
        // Show data
        tvName.setText(name);
        tvAddress.setText(address);
    }

    // Load image of place
    private class ImageDownloadTask extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PlaceDetailActivity.this);
            pDialog.setMessage(getString(R.string.loading));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            try {
                // Starting image download
                bitmapPhoto = Common.downloadImage(url[0]);
            } catch (Exception e) {
                Log.e("Background Task", e.toString());
            }
            return bitmapPhoto;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            pDialog.dismiss();
            bitmapPhoto = bitmap;
            // Show image place
            imgView.setImageBitmap(bitmap);
        }
    }
}
