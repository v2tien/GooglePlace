package api.place.com.placeapi.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by VuVan on 16/02/2017.
 */

public class Constant {

    // KEY Strings
    public static String KEY_REFERENCE = "reference"; // id of the place
    public static String KEY_NAME = "name"; // name of the place
    public static String KEY_VICINITY = "vicinity"; // Place area name
    public static String PLACE_ID = "place_id";
    public static String KEY_ADDRESS = "address";
    public static String KEY_HEIGHT = "height";
    public static String KEY_WIDTH = "width";
    public static String KEY_REFERENCE_PHOTO = "photo_reference";
    public static String KEY_BITMAP = "bitmap";

    public static String TYPE_INTENT= "type_intent";
    public static int TYPE_FROM_MAP = 0;
    public static int TYPE_FROM_FAVORITE = 1;

    public static Context context;
    public static SharedPreferences preferences;
    public static final String PREF_APP = ".preference";
}
