package api.place.com.placeapi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import api.place.com.placeapi.R;
import api.place.com.placeapi.model.Favorite;

/**
 * Created by VuVan on 17/02/2017.
 */

public class FavoriteAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

    private List<Favorite> arr;
    private LayoutInflater inflater;
    private Context context;
    private ViewClickItem viewClickItem;

    // Constructor of adapter
    public FavoriteAdapter(Context context, List<Favorite> arr, ViewClickItem viewClickItem) {
        this.context = context;
        this.arr = arr;
        this.viewClickItem = viewClickItem;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = inflater.inflate(R.layout.item_favorite, null);
        // Listener click item favorite
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView, new RecyclerViewHolders.ViewHolderClicks() {
            @Override
            public void clickFavorite(int position) {
                viewClickItem.clickFavorite(position);
            }
        });
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        // Show data on list favorite
        holder.imvPhoto.setImageBitmap(arr.get(position).getBitmap());
        holder.tvName.setText(arr.get(position).getName());
        holder.tvAddress.setText(arr.get(position).getAddress());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arr.size();
    }

    public void setListFavorite(List<Favorite> favorites) {
        this.arr = favorites;
        notifyDataSetChanged();
    }

    public interface ViewClickItem {
        public void clickFavorite(int position);
    }
}
