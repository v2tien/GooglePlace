package api.place.com.placeapi.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import api.place.com.placeapi.R;
import api.place.com.placeapi.activity.PlaceDetailActivity;
import api.place.com.placeapi.adapter.FavoriteAdapter;
import api.place.com.placeapi.data.GooglePlaces;
import api.place.com.placeapi.model.Favorite;
import api.place.com.placeapi.service.Notify;
import api.place.com.placeapi.util.Common;
import api.place.com.placeapi.util.Constant;

import static api.place.com.placeapi.util.Constant.context;

/**
 * Created by VuVan on 16/02/2017.
 */

public class FavoriteFragment extends Fragment {

    private RecyclerView recyclerView;
    private FavoriteAdapter favoriteAdapter;
    private List<Favorite> favoriteList = new ArrayList<>();
    private TextView tvResult;
    private StaggeredGridLayoutManager layoutManager;

    private Favorite favorites;

    private BroadcastReceiver favorite;
    private BroadcastReceiver unFavorite;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_favorite, container, false);
        initControl(layout);
        registerReceiver();
        return layout;
    }
    // Event listener click item of list favorite
    FavoriteAdapter.ViewClickItem clickItem = new FavoriteAdapter.ViewClickItem() {
        @Override
        public void clickFavorite(int position) {
            Intent intent = new Intent(context, PlaceDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constant.KEY_REFERENCE, favoriteList.get(position).getReference());
            bundle.putString(Constant.KEY_NAME, favoriteList.get(position).getName());
            bundle.putString(Constant.KEY_ADDRESS, favoriteList.get(position).getAddress());
            intent.putExtra(Constant.KEY_REFERENCE_PHOTO, favoriteList.get(position).getReferencePhoto());
            intent.putExtra(Constant.KEY_WIDTH, String.valueOf(favoriteList.get(position).getWidthPhoto()));
            intent.putExtra(Constant.KEY_HEIGHT, String.valueOf(favoriteList.get(position).getHeightPhoto()));
            intent.putExtras(bundle);
            startActivity(intent);
        }
    };

    private void initControl(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        tvResult = (TextView) view.findViewById(R.id.tv_result);

        layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        // Check list favorite empty
        if (favoriteList.isEmpty()) {
            tvResult.setVisibility(View.VISIBLE);
        } else {
            // Show data for recycle view
            tvResult.setVisibility(View.GONE);
            favoriteAdapter = new FavoriteAdapter(getActivity(), favoriteList, clickItem);
            recyclerView.setAdapter(favoriteAdapter);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }

    public void registerReceiver() {
        // Register receiver info action favorite place
        IntentFilter filter = new IntentFilter(Notify.FAVORITE_PLACE.getValue());
        favorite = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String name = intent.getStringExtra(Constant.KEY_NAME);
                String address = intent.getStringExtra(Constant.KEY_ADDRESS);
                String reference = intent.getStringExtra(Constant.KEY_REFERENCE);
                String referencePhoto = intent.getStringExtra(Constant.KEY_REFERENCE_PHOTO);
                int width = intent.getIntExtra(Constant.KEY_WIDTH, 0);
                int height = intent.getIntExtra(Constant.KEY_HEIGHT, 0);
                favorites = new Favorite();
                favorites.setName(name);
                favorites.setAddress(address);
                favorites.setReference(reference);
                favorites.setReferencePhoto(referencePhoto);
                favorites.setWidthPhoto(width);
                favorites.setHeightPhoto(height);

                String Url = GooglePlaces.getLinkPhoto(referencePhoto, width, height);

                new ImageDownloadTask().execute(Url);
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(favorite, filter);

        // Register receiver info action unfavorite place
        IntentFilter filter1 = new IntentFilter(Notify.UNFAVORITE_PLACE.getValue());
        unFavorite = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String name = intent.getStringExtra(Constant.KEY_NAME);
                String address = intent.getStringExtra(Constant.KEY_ADDRESS);
                Favorite unfavorite = new Favorite();
                unfavorite.setName(name);
                unfavorite.setAddress(address);
                for (int i = 0; i < favoriteList.size(); i++) {
                    if (favoriteList.get(i).getName().equalsIgnoreCase(name)) {
                        favoriteList.remove(favoriteList.get(i));
                        favoriteAdapter.setListFavorite(favoriteList);
                        if (favoriteList.isEmpty()) {
                            tvResult.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(unFavorite, filter1);
    }

    public void unregisterReceiver() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(favorite);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(unFavorite);
    }

    private class ImageDownloadTask extends AsyncTask<String, String, Bitmap> {
        Bitmap bitmap = null;
        @Override
        protected Bitmap doInBackground(String... url) {
            try{
                // Starting image download
                bitmap = Common.downloadImage(url[0]);
            }catch(Exception e){
                Log.e("Background Task", e.toString());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            favorites.setBitmap(result);
            // Add place to list favorite
            favoriteList.add(favorites);
            // Check adapter and show data favorite
            if(favoriteAdapter == null){
                favoriteAdapter = new FavoriteAdapter(getActivity(), favoriteList, clickItem);
                recyclerView.setAdapter(favoriteAdapter);
            }else {
                favoriteAdapter.setListFavorite(favoriteList);
            }
            tvResult.setVisibility(View.GONE);
        }
    }
}
